

import XCTest

class NewyorkUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testProgressSpinnerIsHiddenOnError()
    {
        let app = XCUIApplication()
        app.alerts["Error"].buttons["ok"].tap()
        let inProgressActivityIndicator = app.tables["In progress"].activityIndicators["In progress"]
        XCTAssertFalse(inProgressActivityIndicator.exists)
        
    }

    func testSearchMissingCredentialAlertIsShown()
    {
        
        let app = XCUIApplication()
        app.navigationBars["NY Times"].buttons["Search"].tap()
        
        let searchAlert = app.alerts["Search"]
        searchAlert.collectionViews.textFields["search..."].tap()
        searchAlert.buttons["search"].tap()
        
        let alertDialog = app.alerts["Error"]
        
        XCTAssertTrue(alertDialog.exists)
        
        alertDialog.buttons["ok"].tap()

    }

}

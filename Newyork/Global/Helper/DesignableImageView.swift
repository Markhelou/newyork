import UIKit

@IBDesignable class DesignableImageView:UIImageView
{
    let imageCache = NSCache<AnyObject, AnyObject>()
    var imageUrlString : String?
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = true
        }
    }
        
    func loadFromUrl(url_str:String){
        let url:URL = URL(string: url_str)!
        let session = URLSession.shared
        
        imageUrlString = url_str
        
        if let imageFromCatch = imageCache.object(forKey: url_str as AnyObject) as? UIImage
        {
            self.image = imageFromCatch
            return
        }
        
        let task = session.dataTask(with: url, completionHandler: {
            (
            data, response, error) in
            
            
            if data != nil
            {
                let imageToCache = UIImage(data: data!)
                
                if(imageToCache != nil)
                {
                    DispatchQueue.main.async(execute: {
                        
                        if self.imageUrlString == url_str {
                            self.image = imageToCache
                        }
                        self.imageCache.setObject(imageToCache!, forKey: url_str as AnyObject)
                        
                    })
                    
                }
                
            }
        })
        task.resume()
    }

}

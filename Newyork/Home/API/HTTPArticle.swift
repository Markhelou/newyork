
import UIKit

class HTTPArticle {
    private init() {}
    static let shared = HTTPArticle()
    
    
    func GetArticlesAction(URLString:String ,completion: @escaping(Sections?,Error?) ->()){
        
        guard let url = URL(string: URLString) else
        {
            completion(nil, NSError(domain:"", code:0, userInfo:nil))
            return
            
        }
        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            
            guard let responseStatus = response as? HTTPURLResponse , responseStatus.statusCode == 200
                else
            {
                completion(nil,error)
                return
            }
            
            guard let data = data , error == nil
                else {
                    completion(nil,error)
                    return
            }
            
            do
            {
                let sections = try JSONDecoder().decode(Sections.self, from: data)
                completion(sections,nil)
            }
            catch let jsonError
            {
                completion(nil,jsonError)
            }
        }
        
        task.resume()
    }
    
}

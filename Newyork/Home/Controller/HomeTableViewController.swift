
import UIKit

class HomeTableViewController: UITableViewController {
    
    private let cellIdentifier = "NYMostPopular"
    private var articles:[Article]?
    private var refresher:UIRefreshControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(refreshPage), for: UIControl.Event.valueChanged)
        tableView.addSubview(refresher)
        
        spinner.hidesWhenStopped = true
        spinner.startAnimating()
        tableView.backgroundView = spinner
        
        
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableView.automaticDimension
        
        sendArticleRequest {
            DispatchQueue.main.async {
                spinner.stopAnimating()
            }
        }
    }
    
    private func sendArticleRequest(completion: @escaping ()->())
    {
        HTTPArticle.shared.GetArticlesAction(URLString: MAIN_URL) {[weak self] (sections, error) in
            var msg = APIError.unkwown
            if let err = error {
                
                if let error = err as NSError?, error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
                    msg = APIError.noConnection
                }
                DispatchQueue.main.async {
                    self?.addAlert(msg: msg)
                }
                completion()
                return
            }
            if let section = sections , let status = SectionsViewModel(section: section).status , status == true
            {
                self?.articles = sections?.results
                
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }
            else
            {
                DispatchQueue.main.async {
                    self?.addAlert(msg: msg)
                }
            }
            
            completion()
            
        }
    }
    
    @objc func refreshPage(){
        sendArticleRequest {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.refresher.endRefreshing()
            }
        }
    }
    
    private func addAlert(msg:String)
    {
        let alert = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "ok", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true)
    }
    
    
    //tableView Content
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! NYMostPopularTableViewCell
        
        cell.article = articles?[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = StoryBoard.Home.instantiateViewController(withIdentifier: StoryBoard.controller.DetailsVC) as? DetailsViewController {
            vc.article = articles?[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
